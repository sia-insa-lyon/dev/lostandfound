// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,  
  URL_API : 'http://localhost:3000/',  
  objectsMap: ["Carte", "Téléphone", "Clefs", "Sac à dos", "Portefeuille", "Autres"],
  dateMap: ["Aujourd'hui", "Hier", "Derniers 3 jours", "Cette semaine", "Jour spécifique", "Après le (compris)", "Avant le (compris)"],
  latitudeMap : 45.7832718,
  longitudeMap : 4.877895,
  objectTTL: 30,
  CAPTCHA_WEB_KEY:'6Lf6k-AZAAAAALaawD6nn-UdMfN5OKRgwCOFYCQQ',
  flagsToShow: ['al', 'ad', 'at', 'by', 'be', 'ba', 'bg', 'hr', 'cz','ch', 'dk',
    'ee', 'fo', 'fi', 'fr', 'de', 'gi', 'gr', 'va', 'hu', 'is', 'ie', 'it', 'lv',
    'li', 'lt', 'lu', 'mk', 'mt', 'md', 'mc', 'me', 'nl', 'no', 'pl', 'pt', 'ro',
    'ru', 'sm', 'rs', 'sk', 'si', 'es', 'se', 'ch', 'ua', 'gb'] //SETTING ALL THE FALGS DISPONIBLES TO DIAL INPUT
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
