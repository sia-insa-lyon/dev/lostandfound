export const environment = {
  production: true,  
  objectsMap: ["Carte", "Téléphone", "Clefs", "Sac à dos", "Portefeuille", "Autres"],
  dateMap: ["Aujourd'hui", "Hier", "Derniers 3 jours", "Cette semaine", "Jour spécifique", "Après le (compris)", "Avant le (compris)"],
  URL_API : 'http://192.168.0.101:3000/',  
  latitudeMap : 45.7832718,
  longitudeMap : 4.877895,
  emailContact: 'lost-n-found@gmail.com',
  emailSubject: 'Récuperation du compte',
  flagsToShow: ['al', 'ad', 'at', 'by', 'be', 'ba', 'bg', 'hr', 'cz','ch', 'dk',
    'ee', 'fo', 'fi', 'fr', 'de', 'gi', 'gr', 'va', 'hu', 'is', 'ie', 'it', 'lv',
    'li', 'lt', 'lu', 'mk', 'mt', 'md', 'mc', 'me', 'nl', 'no', 'pl', 'pt', 'ro',
    'ru', 'sm', 'rs', 'sk', 'si', 'es', 'se', 'ch', 'ua', 'gb'] //SETTING ALL THE FALGS DISPONIBLES TO DIAL INPUT

};
