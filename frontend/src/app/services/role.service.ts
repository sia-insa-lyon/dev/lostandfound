import { environment } from './../../environments/environment';
import { Role } from './../models/role';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { FormGroup } from '@angular/forms';
@Injectable({
  providedIn: 'root'
})
export class RoleService {
  selectedRole:Role;
  moderators:Role[];
  message: string;
  private URL_API = environment.URL_API;
  constructor(private http: HttpClient) { }


  //LOG IN
  postLogin(role: Role) {
    return this.http.post(this.URL_API + 'signin', role, {
      observe: 'body',
      withCredentials: true,
      headers: new HttpHeaders().append('Content-Type', 'application/json')
    })

  }

  //GET ALL MODERATORS
  getModerators(){
    return this.http.get(this.URL_API+'roles', {
      observe: 'body',
      withCredentials: true,
      headers: new HttpHeaders().append('Content-Type', 'application/json')
    });
  }


  //GET ALL ROLES
  getRole(){
    return this.http.get(this.URL_API + 'role', {
      observe: 'body',
      withCredentials: true,
      headers: new HttpHeaders().append('Content-Type', 'application/json')
    })
  }


  //CREATE A MODERATOR
  postModerator(moderator: Role){
    return this.http.post(this.URL_API + 'roles/add', moderator, {
      observe: 'body',
      withCredentials: true,
      headers: new HttpHeaders().append('Content-Type', 'application/json')
    }) 
  }


  //DELETE A MODERATOR
  deleteModerator(_id:string){
    return this.http.delete(this.URL_API+'roles/delete/'+_id,{
      observe:'body',
      withCredentials: true,
      headers: new HttpHeaders().append('Content-Type','application/json')
    });
  }


  //CHANGE MODERATOR'S OR ADMIN'S PASSWORD
  changePassword(passwordForm: FormGroup){
    return this.http.put(this.URL_API + 'roles/changePassword',passwordForm , {
      observe: 'body',
      withCredentials: true,
      headers: new HttpHeaders().append('Content-Type', 'application/json')
    })
  }

  getToken() {
    return localStorage.getItem('token');
  }

  //CHECKS TOKEN IF EXISTS
  loggedIn() {
    return !!localStorage.getItem('token');//If THE TOKEN EXISTS will return true
  }





}
