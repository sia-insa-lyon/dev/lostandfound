import { environment } from './../../environments/environment';

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private ROLE: string;
  private LOGGED = false;
  private USER_STATUS = "";
  private URL_API = environment.URL_API;
  constructor(private http: HttpClient, private router: Router) { }

  getConnection() {//Get access (if user o manager logged in)
    return this.http.get(this.URL_API + 'access', {
      observe: 'body',
      withCredentials: true,
      headers: new HttpHeaders().append('Content-Type', 'application/json')
    })
  }

  loggedIn() {
    return this.LOGGED
  }

  login() {
    this.LOGGED = true;
  }

  logout() {//Log out an entity
    localStorage.removeItem('token');
    localStorage.removeItem('role');
    this.LOGGED = false;
    this.ROLE = '';
    return this.http.get(this.URL_API + 'logout', {
      observe: 'body',
      withCredentials: true,
      headers: new HttpHeaders().append('Content-Type', 'application/json')
    })

    
  }

  getToken() {
    return localStorage.getItem('token');
  }

  setRole(role: string){
    this.ROLE = role;
  }


  getRole(){
    return localStorage.getItem('role');
  }

  setStatus(status: string){
    this.USER_STATUS = status;
  }

  getStatus(){
    return this.USER_STATUS;
  }


}
