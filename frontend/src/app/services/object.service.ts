import { environment } from './../../environments/environment';
import { Objet } from './../models/object';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';


@Injectable({
  providedIn: 'root'
})
export class ObjectService {
  selectedObject:Objet;
  objects:Objet[];
  message: string;
  flagsToShow: string[];
  private URL_API = environment.URL_API;
  
  constructor(private http:HttpClient) { 
    this.selectedObject = new Objet();
    this.flagsToShow = environment.flagsToShow; 

  }

  //GET CONNECTION (IF USER LOGGED IN BEFORE CONTINUE)
  getConnection(){
    return this.http.get(this.URL_API+'object-found',{
      observe:'body',
      withCredentials: true,
      headers: new HttpHeaders().append('Content-Type','application/json')
    })
  }



  //PUT IN FORM FORMAT A REGISTRATION FORM
  createObjectForm(userForm: FormGroup, file:File){//In order to send a file with json content
    const fd = new FormData();
    console.log(userForm.controls['type'].value)
    fd.append('type',userForm.controls['type'].value);
    fd.append('latitude',userForm.controls['latitude'].value);
    fd.append('longitude',userForm.controls['longitude'].value);
    fd.append('description',userForm.controls['description'].value);
    fd.append('name',userForm.controls['name'].value);
    fd.append('email',userForm.controls['email'].value);
    fd.append('confirmEmail',userForm.controls['confirmEmail'].value);
    fd.append('image', file);
    return fd;
  }


  //POST AN OBJECT
  postObject(userForm: FormGroup,file:File){
    const objet = this.createObjectForm(userForm,file);
    return this.http.post(this.URL_API+`object-found/${userForm.controls['recaptcha'].value}`,objet,{
      observe:'body',
      withCredentials: true,
    });
  }


  //GET ALL THE OBJECTS BY SETTING A FILTER
  getObjects(page: Number, filter:any){
    return this.http.put(this.URL_API+'object-lost/'+page,{filter:filter},{
      observe:'body',
      withCredentials: true,
      headers: new HttpHeaders().append('Content-Type','application/json')
    })
  }

  //GET ALL THE OBJECTS WITH THEIR USERS BY SETTING A FILTER
  getObjectsWithUsers(page: Number, filter:any){
    return this.http.put(this.URL_API+'manager/objects/'+page,{filter:filter},{
      observe:'body',
      withCredentials: true,
      headers: new HttpHeaders().append('Content-Type','application/json')
    })
  }

  //GET ALL THE USER'S OBJECTS
  getObjectsUser(user:string){
    return this.http.get(this.URL_API+'user/objects/'+user,{
      observe:'body',
      withCredentials:true,
      headers:new HttpHeaders().append('Content-Type','application/json')
    });
  }


  //DELETE AN OBJECT
  deleteObject(_id:string){
    return this.http.delete(this.URL_API+'user/objects/'+_id,{
      observe:'body',
      withCredentials: true,
      headers: new HttpHeaders().append('Content-Type','application/json')
    });
  }


  //EDIT AN OBJECT
  putObject(objet : Objet){
    return this.http.put(this.URL_API+'user/objects/'+objet._id,objet,{
      observe:'body',
      withCredentials: true,
      headers: new HttpHeaders().append('Content-Type','application/json')
    });
  }


  //DECLARE AN OBJECT AS FOUND BY USER
  objectFound(code:string){
    return this.http.delete(this.URL_API+'object/found/'+code,{
      observe:'body',
      withCredentials: true,
      headers: new HttpHeaders().append('Content-Type','application/json')
    });
  }


  contactPerson(_id:String,emailForm: FormGroup){
    return this.http.put(this.URL_API+'contact/'+_id,emailForm.value,{
      observe:'body',
      withCredentials: true,
      headers: new HttpHeaders().append('Content-Type','application/json')
    });
  }

  
}
