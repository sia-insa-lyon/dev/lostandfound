import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class StatisticsService {
  private URL_API = environment.URL_API;

  constructor(private http:HttpClient) { }

  //GET STATISTICS OF OBJECTS
  getObjectStatistics(){
    return this.http.get(this.URL_API+'statistics',{
      observe:'body',
      withCredentials: true,
      headers: new HttpHeaders().append('Content-Type','application/json')
    })
  }

}
