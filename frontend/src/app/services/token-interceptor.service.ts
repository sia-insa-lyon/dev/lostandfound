import { RoleService } from 'src/app/services/role.service';
import { Injectable } from '@angular/core';
import { HttpInterceptor } from '@angular/common/http'


@Injectable({
  providedIn: 'root'
})
export class TokenInterceptorService implements HttpInterceptor {

  constructor(private roleService: RoleService) { }

  //ADD AUTHORIZATION HEADER WITH THE RECEIVED TOKEN TO ANY REQUEST FROM ANGULAR
  intercept(req, next) {
    let tokenizeReq = req.clone({
      setHeaders: {
        Authorization: `Bearer ${this.roleService.getToken()}`
      }
    });
    return next.handle(tokenizeReq);
  }

}