import { RoleService } from 'src/app/services/role.service';
import { AuthService } from './../services/auth.service';
import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(
    private roleService: RoleService,
    private authService: AuthService,
    private router: Router) {

  }
  //Manage access to any route on front-end
  canActivate(route: ActivatedRouteSnapshot): boolean {
    //console.log('Variable: ' + this.authService.loggedIn());
    //console.log('Token: ' + this.userService.loggedIn());
    if (route.data.role.includes('NONE')) {
      return true;
    }
    if (this.roleService.loggedIn()/* && this.authService.loggedIn()*/) {
      if (route.firstChild) {//Validate child management routes
        if (route.firstChild.data.role.includes(this.authService.getRole())) {
          return true;
        } else {
          this.router.navigate(['/object-lost']);
          return false;
        }
      }else {
        if (route.data.role.includes(this.authService.getRole())) {
          return true;
        } else {
          this.router.navigate(['/object-lost']);
          return false;
        }
      }
    }

    this.router.navigate(['/object-lost']);
    return false;
  }
}
