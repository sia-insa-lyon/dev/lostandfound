//DON'T READ THIS DOCUMENT
import { Component, OnInit } from '@angular/core';
//Leaflet Map
import { latLng, tileLayer,marker, icon ,Map, Layer} from 'leaflet';
import { environment } from './../../../environments/environment';

import * as $ from "jquery";
@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.scss'],
  
})
export class TestComponent implements OnInit {
  SITE_KEY = environment.CAPTCHA_WEB_KEY;
  //Leaflet Map Settings
  map: Map;
  latLng: any;

  //Mostrando mapa
  showContent: Boolean;

  markerLocation = marker([ 45.7833922,4.877691 ], {
    icon: icon({
      iconSize: [ 25, 41 ],
      iconAnchor: [ 13, 41 ],
      iconUrl: 'leaflet/marker-icon.png',
      iconRetinaUrl: 'leaflet/marker-icon-2x.png',
      shadowUrl: 'leaflet/marker-shadow.png'
    })
  });


  layersControl = {
    overlays: {
      'Objet trouvé ici.': this.markerLocation
    }
  };

  layer = this.markerLocation;
  options = {
    layers: [
      tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; OpenStreetMap contributors'
      }),this.markerLocation
    ],
    zoom: 17,
    minZoom: 15,
    center: latLng([ 45.7832718, 4.877895 ])
  };

  constructor() { 
    this.showContent = false;
    this.latLng = [45.7833922,4.877691];
  }

  ngOnInit(): void {
    console.log(this.SITE_KEY)
  }

  changeContent(valor :Boolean){
      this.showContent = valor;
      if(!this.showContent){
        this.markerLocation.setLatLng([-1,-1]);
      }else{
        this.markerLocation.setLatLng(this.latLng);
      }

  }

  showMap(){
    if (this.map) {
      // this.streetMaps.redraw();
      $('#exampleModal').on('show.bs.modal', function(){
        setTimeout(function() {
          this.map.invalidateSize();
        }, 10);
       });
    }
  }

  refreshMap() {
    if (this.map) {
       //this.map.redraw();
      this.map.invalidateSize();
    }
  }

  onMapReady(map: L.Map) {
    console.log('Aqui')
    map.on('click', (eventMouse: L.LeafletMouseEvent) => {
      this.latLng = [eventMouse.latlng.lat, eventMouse.latlng.lng];
      map.setView(this.latLng, map.getZoom());
    });
    this.map = map;
  }

  clickMap(event){
    if(!this.showContent){
      console.log(event);
      console.log(event.latlng.lat,event.latlng.lng);
      this.markerLocation.setLatLng(event.latlng);
    }

  }

  //recaptcha
  resolved(captchaResponse: string) {
    console.log(`Resolved response token: ${captchaResponse}`);
   
  }


}
