import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { environment } from './../../../environments/environment';
import { AuthService } from './../../services/auth.service';
import { Router } from '@angular/router';
import { Objet } from './../../models/object';
import { ObjectService } from './../../services/object.service';
import { Component, OnInit, AfterViewInit } from '@angular/core';

//import { isAfter, isBefore, isEqual, isWithinInterval, sub } from 'date-fns'
import { latLng, tileLayer, marker, icon, Map } from 'leaflet';//Leaflet Map

/*End Open street Map*/
@Component({
  selector: 'app-object-lost',
  templateUrl: './object-lost.component.html',
  styleUrls: ['./object-lost.component.scss'],
  providers: [ObjectService]
})
export class ObjectLostComponent implements OnInit, AfterViewInit {

  //Content variables
  filter_arr = [];
  arrayAAfficher = [];
  dateMap: string[];
  objectsMap: string[];
  selectedObject: Objet;

  //Mail form
  emailForm: FormGroup;
  isCheckedPhoneNumber: boolean;
  dialCode = "+33";
  SITE_KEY = environment.CAPTCHA_WEB_KEY;
  errors: string[];

  //Map settings
  latitude = environment.latitudeMap;
  longitude = environment.longitudeMap;
  zoom = 18;

  map: Map;
  latLng: any;
  markerLocation = marker([this.latitude, this.longitude], {
    icon: icon({
      iconSize: [25, 41],
      iconAnchor: [13, 41],
      iconUrl: 'leaflet/marker-icon.png',
      iconRetinaUrl: 'leaflet/marker-icon-2x.png',
      shadowUrl: 'leaflet/marker-shadow.png'
    })
  }).bindPopup('<b>Trouvé ici</b>')
    .openPopup();;

  layersControl = {
    overlays: {
      'Objet trouvé ici.': this.markerLocation
    }
  };

  options = {
    layers: [
      tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; OpenStreetMap contributors'
      }), this.markerLocation
    ],
    zoom: 17,
    minZoom: 15,
    center: latLng([this.latitude, this.longitude])
  };

  //Selected object variables
  private rootAddresePhoto;
  selectedType = -1;
  selectedDescription = [""];
  selectedDate = '1968-11-16T00:00:00';
  modalPath: string;
  modalDescription: string;


  //Filter variables
  objectFilterParameter: number;
  dateFilterParameter: number;
  descriptionFilterParameter: string;
  calendarPicker: Date;
  calendar: Date;
  now: Date;
  objectDate: Date;

  //Pagination variables
  loading: Boolean;
  hasPrevPage: Boolean;
  hasNext: Boolean;
  currentPage: number;
  fakeArrayTotalPages: number[];


  constructor(public objectService: ObjectService, private router: Router, public authService: AuthService, private fb: FormBuilder) {
    this.getObjects();
    this.createMailForm();
    this.errors =[];
    this.isCheckedPhoneNumber = false;
    this.dateMap = environment.dateMap;
    this.objectsMap = environment.objectsMap;
    this.loading = true;
    this.rootAddresePhoto = environment.URL_API;
    this.objectFilterParameter = -1;
    this.dateFilterParameter = -1;
    this.descriptionFilterParameter = "";
    this.calendarPicker = new Date();
    this.hasPrevPage = true;
    this.selectedObject = new Objet();
    this.hasNext = true;
    this.currentPage = 1;
    this.fakeArrayTotalPages = [];

  }
  ngAfterViewInit(): void {

  }

  ngOnInit(): void {

  }

  createMailForm() {
    this.emailForm = this.fb.group({
      email: ['', [Validators.required, Validators.pattern("[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}")]],
      confirmEmail: ['', Validators.required],
      number: [{ value: '', disabled: !this.isCheckedPhoneNumber }, [Validators.required, Validators.pattern('([0-9 ])+')]],
      dial: [this.dialCode],
      recaptcha:[null,[Validators.required]]
    });
  }

  // Convenience getter for easy access to form fields
  get f() { return this.emailForm.controls; }


  //Cleans filter
  cleanFilter() {
    this.dateFilterParameter = -1;
    this.descriptionFilterParameter = "";
    this.objectFilterParameter = -1;
    this.arrayAAfficher = this.objectService.objects;
  }

  //Create a filter
  filter() {
    if (!this.checkFilterChanges()) {
      this.currentPage = 1;
    }
    this.filter_arr = [];
    if (this.objectFilterParameter != -1) {
      this.filter_arr.push(['object', this.objectFilterParameter]);
    }
    if (this.dateFilterParameter != -1) {
      this.filter_arr.push(['date', this.dateFilterParameter, this.calendarPicker]);
    }
    if (this.descriptionFilterParameter != "") {
      this.filter_arr.push(['description', this.descriptionFilterParameter]);
    }
    this.getObjects();
  }

  //Objet type Filter
  objectFilter = (object: any) => {
    return this.objectFilterParameter == Number(object.type);
  }


  //Description filter
  descriptionFilter = (object: any) => {
    var descriptionSplit = this.descriptionFilterParameter.split(" ");
    return descriptionSplit.every(item_str => object.description.includes(item_str));
  }


  isToday(someDate: Date) {
    const today = new Date()
    return someDate.getDate() == today.getDate() &&
      someDate.getMonth() == today.getMonth() &&
      someDate.getFullYear() == today.getFullYear()
  }



  getAddresIP() {
    return this.rootAddresePhoto;
  }

  getObjects() {
    this.loading = true;
    this.objectService.getObjects(this.currentPage, this.filter_arr)
      .subscribe(res => {
        this.authService.login();
        this.objectService.objects = res['docs'] as Objet[];
        this.fakeArrayTotalPages = new Array(res['totalPages']);
        this.hasPrevPage = res['hasPrevPage'];
        this.hasNext = res['hasNextPage'];
        this.currentPage = res['page'];
        this.arrayAAfficher = this.objectService.objects;
  
        this.loading = false;
        this.sendToTop();
      },
        error => {
          this.authService.logout();
          this.router.navigate(['/object-lost'])
        }
      )
  };



  showMap(latitude: number, longitude: number) {//Open map with coordinates
    this.markerLocation.setLatLng([latitude, longitude]);
    this.map.setView([latitude, longitude], this.map.getZoom());
    this.latitude = latitude;
    this.longitude = longitude;
  }

  refreshMap() {
    if (this.map) {
      // this.streetMaps.redraw();
      this.map.invalidateSize();
    }
  }

  onMapReady(map: L.Map) {
    map.on('click', (eventMouse: L.LeafletMouseEvent) => {
      this.latLng = [eventMouse.latlng.lat, eventMouse.latlng.lng];
      map.setView(this.latLng, map.getZoom());
    });
    this.map = map;
  }


  previewImage(modalPath: string, description: string) {
    this.modalPath = modalPath;
    this.modalDescription = description;
  }

  loadMoreObjects(page: number) {
    this.currentPage = page;
    this.getObjects();
  }

  checkFilterChanges() {
    let verificator = 0;
    this.filter_arr.forEach((filter) => {
      if (filter[0] == "date") {
        if (filter[1] == this.descriptionFilterParameter && filter[2] == this.calendarPicker) {
          verificator++;
        }
      }
      if (filter[0] == "description") {
        if (filter[1] == this.descriptionFilterParameter) {
          verificator++;
        }
      }
      if (filter[0] == "object") {
        if (filter[1] == this.objectFilterParameter) {
          verificator++;
        }
      }
    });

    return verificator == this.filter_arr.length;

  }


  //////////////////

  onCountryChange(event) { //Select country dial code
    this.dialCode = "+" + event.dialCode;
    this.emailForm.controls['dial'].setValue(this.dialCode);
  }

  numericOnly(event) {//Number validation
    let patt = /^([0-9])$/;
    let result = patt.test(event.key);
    const long = this.f.number.value.length;
    if (long == 14) {
      return false;
    }
    return result;
  }

  checkValue(event: boolean) { //Check boxes value
    const number = this.emailForm.get('number');
    //console.log(event)
    if (!event) {
      this.isCheckedPhoneNumber = true;
      number.enable();
      //this.emailForm.controls['contact'].setValue(["email", "number"]);
    } else {
      this.isCheckedPhoneNumber = false;
      number.disable();
      //this.emailForm.controls['contact'].setValue(["email"]);
    }
  }

  emailVal(event) {//Email validation
    let patt = /^([a-z]|[.-@]|\d)$/;
    let result = patt.test(event.key);
    return result;
  }

  sendMail() {
    this.errors = [];
    if (!this.emailForm.invalid) {
      //console.log(this.emailForm)
      this.objectService.contactPerson(this.selectedObject._id, this.emailForm)
        .subscribe(res => {
          if(res['success']){
            document.getElementById('cancelSendMail').click();
          }else{
            this.errors = res['errors'];
            //console.log(this.errors)
          }
        },
          error => {
            //this.authService.logout();
          }
        )
    }
  }

  sendToTop() {
    let scrollToTop = window.setInterval(() => {
      let pos = window.pageYOffset;
      if (pos > 0) {
        window.scrollTo(0, pos - 20); // how far to scroll on each step
      } else {
        window.clearInterval(scrollToTop);
      }
    }, 16);
  }

  setSelectedObject(id: string) {
    this.selectedObject._id = id;
    this.createMailForm();
  }

  //recaptcha
  resolved(captchaResponse: string) {
    //console.log(`Resolved response token: ${captchaResponse}`);

  }

  resetForm(){
    this.emailForm.reset();
  }
}


/******************************************TESTS***************** */

