import { environment } from './../../../environments/environment';
import { AuthService } from './../../services/auth.service';
import { ObjectService } from './../../services/object.service';
import { Component, OnInit, AfterViewInit, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, ValidationErrors, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgxImageCompressService } from 'ngx-image-compress';// Image compression
declare var jQuery: any;

import { latLng, tileLayer, marker, icon, Map } from 'leaflet';//Leaflet Map

@Component({
  selector: 'app-object-found',
  templateUrl: './object-found.component.html',
  styleUrls: ['./object-found.component.scss'],
  providers: [ObjectService]
})

export class ObjectFoundComponent implements OnInit, AfterViewInit {
  @ViewChild('confirmationModal') confirmationModal: ElementRef;

  //Form Variables
  objectForm: FormGroup;
  SITE_KEY = environment.CAPTCHA_WEB_KEY;

  //Leaflet Map Settings
  map: Map;
  latLng: any;
  latitude = environment.latitudeMap;//Initial latitude
  longitude = environment.longitudeMap;//Initial longitude
  locationChosen = false;
  zoom = 15;
  minZoom = 10;

  markerLocation = marker([-1, -1], {
    icon: icon({
      iconSize: [25, 41],
      iconAnchor: [13, 41],
      iconUrl: 'leaflet/marker-icon.png',
      iconRetinaUrl: 'leaflet/marker-icon-2x.png',
      shadowUrl: 'leaflet/marker-shadow.png'
    })
  });

  layersControl = {
    overlays: {
      'Objet trouvé ici.': this.markerLocation
    }
  };

  options = {
    layers: [
      tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; OpenStreetMap contributors'
      }), this.markerLocation
    ],
    zoom: 17,
    minZoom: 15,
    center: latLng([this.latitude, this.longitude])
  };


  // General variables
  isChecked = false;
  objectsMap: string[];
  typeT: string;//Object type
  emailstring: string;



  //Errors
  errors = [];

  //Image Compression variables
  file: File;//image
  localCompressedURl: string;
  maxHeight = 400;
  maxWidth = 600;

  constructor(public objectService: ObjectService, private router: Router, public authService: AuthService, private fb: FormBuilder, private imageCompress: NgxImageCompressService) {
    //this.Connection();
    this.createForm();
    this.typeT = "0";
    this.file = undefined;
    this.objectsMap = environment.objectsMap;
  }


  ngOnInit(): void {

  }

  ngAfterViewInit(): void {

  }

  createForm() {//New user form
    this.objectForm = this.fb.group({
      type: ['-1', Validators.required],
      longitude: [null, this.locationChosen],
      latitude: [null,this.locationChosen],
      description: ['', Validators.required],
      file: [{ value: null, disabled: this.isChecked }],
      name: ['',[ Validators.required,/* Validators.pattern("([0-9\@_.*\\-\$\!\%\#\+\\t])+")*/]],
      email: ['', [Validators.required, Validators.pattern("[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}")]],
      confirmEmail: ['', Validators.required],
      recaptcha:[null,[Validators.required]]
    });
  }

  // Convenience getter for easy access to form fields
  get f() { return this.objectForm.controls; }

  resetForm(form?: NgForm) {//Resenting form
    form.reset();
  }


  validationData() {//Validate information before sending to server
    this.errors = null;
    this.errors = [];

    if (!this.locationChosen) {
      this.errors.push("Insérez une localisation");
      document.getElementById("mapPanel").style.borderColor = "red";
    } else {
      document.getElementById("mapPanel").style.borderColor = "green";
    }
    if (!this.file && this.isChecked) {
      this.errors.push("Insérez une image")
      document.getElementById("inputPhoto").style.borderColor = "red";
    } else {
      document.getElementById("inputPhoto").style.borderColor = "green";

    }
    if (this.objectForm.invalid) {
      //console.log("error")
      this.errors.push("Veuillez compléter toutes les informations requises");
    }
    if (this.errors.length == 0) {
      this.addObject();
    } else {
      //this.getFormValidationErrors();
      this.sendToTop();
    }
  }

  addObject() {//Save an object to the database
     this.objectService.postObject(this.objectForm,this.file)
       .subscribe(
         res => {
           //console.log(res)
           if(!res['success']){
             this.errors = res['errors'];
           }else{
            jQuery(this.confirmationModal.nativeElement).modal('show');
           }
           
         },
         error => console.log(error))
  };

  closeModal() {
    this.router.navigate(['/object-lost']);
  }

  onItemChange(value) {//Change type value
    this.typeT = value;
  }

  clickMap(event) {//Change map localisation
    this.markerLocation.setLatLng(event.latlng);
    this.latitude = event.latlng.lat;
    this.longitude = event.latlng.lng;
    this.f.longitude.setValue(this.longitude);
    this.f.latitude.setValue(this.latitude);
    this.locationChosen = true;
  }

  onPhotoSelected(event): void {//Loading image
    if (event.target.files && event.target.files[0]) {
      //console.log(event.target.files[0]['size'] / (1024 * 1.024));
      if (event.target.files[0]['size'] / (1024 * 1.024) <= 5000) {//we get the size in KB
        this.file = <File>event.target.files[0];
        //image preview
        const reader = new FileReader();
        reader.onload = (event: any) => {
          //Initialize the JavaScript Image object.
          var image = new Image();
          image.src = event.target.result;
          image.onload = (event: any) => {//get width and height
            let loadedImage = event.currentTarget;
            let width = loadedImage.width;
            let height = loadedImage.height;
            if (width > this.maxWidth || height > this.maxHeight) {//size validation
              this.compressFile(image.src, this.file['name'], width, height)//Compress image
            } else {
              this.localCompressedURl = image.src;
            }
          }
        };

        //console.log(event.target.files[0]['size'] / (1024 * 1.024));
        reader.readAsDataURL(this.file);
      } else {
        console.log("File too big")
      }
    }
  }


  compressFile(image, fileName, width, height) {//Image compression
    var orientation = -1;
    var widthProportion;
    var heightProportion;
    if (height > width) {//vertical photo proportion
      heightProportion = this.maxHeight / height;
      widthProportion = heightProportion;
    } else {//horizontal photo proportion
      widthProportion = this.maxWidth / width;
      heightProportion = widthProportion;
    }
    //console.log(widthProportion * 100);
    if (widthProportion * 100 < 35) {//improve big images quality
      widthProportion = .5;/*widthProportion*4*/
      heightProportion = widthProportion;
    }
    this.imageCompress.compressFile(image, orientation, widthProportion * 100, heightProportion * 100).then(//compression
      result => {
        const imgResultAfterCompress = "" + result;
        this.localCompressedURl = result;
        // create file from byte
        const imageName = fileName;
        // call method that creates a blob from dataUri
        const imageBlob = this.dataURItoBlob(imgResultAfterCompress.split(',')[1]);
        this.file = new File([imageBlob], imageName, { type: 'image/png' });
        this.f.file.setValue(this.file);
        //console.log("file size:", this.file['size'] / (1024 * 1024));
      }
    );
  }

  dataURItoBlob(dataURI) {//Data convertion
    const byteString = window.atob(dataURI);
    const arrayBuffer = new ArrayBuffer(byteString.length);
    const int8Array = new Uint8Array(arrayBuffer);
    for (let i = 0; i < byteString.length; i++) {
      int8Array[i] = byteString.charCodeAt(i);
    }
    const blob = new Blob([int8Array], { type: 'image/jpeg' });
    return blob;
  }


  getCheckBox(event) {//If user adds an image
    this.isChecked = event as boolean;
    if (!this.isChecked) {
      this.localCompressedURl = undefined;
      this.file = undefined;
      this.f.image.setValue(null);
    }
  }

  refreshMap() {
    if (this.map) {
      this.map.invalidateSize();
    }
  }

  onMapReady(map: L.Map) {
   // console.log('Val: ' + this.latLng)
    map.on('click', (eventMouse: L.LeafletMouseEvent) => {
      this.latLng = [eventMouse.latlng.lat, eventMouse.latlng.lng];
      //console.log('Val: ' + this.latLng)
      map.setView(this.latLng, map.getZoom());
    });
    this.map = map;
  }

  sendToTop() {
    let scrollToTop = window.setInterval(() => {
      let pos = window.pageYOffset;
      if (pos > 0) {
        window.scrollTo(0, pos - 20); // how far to scroll on each step
      } else {
        window.clearInterval(scrollToTop);
      }
    }, 16);
  }

  resolved(captchaResponse: string) {
    //console.log(`Resolved response token: ${captchaResponse}`);

  }



  emailVal(event) {//Email validation
    let patt = /^([a-z]|[.-@]|\d)$/;
    let result = patt.test(event.key);
    return result;
  }

  getFormValidationErrors() {
    Object.keys(this.objectForm.controls).forEach(key => {

      const controlErrors: ValidationErrors = this.objectForm.get(key).errors;
      if (controlErrors != null) {
        Object.keys(controlErrors).forEach(keyError => {
          //console.log('Key control: ' + key + ', keyError: ' + keyError + ', err value: ', controlErrors[keyError]);
        });
      }
    });
  }


}
