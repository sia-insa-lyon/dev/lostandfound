import { RoleService } from 'src/app/services/role.service';
import { AuthService } from './../../services/auth.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';


@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss'],
  providers: [RoleService]
})
export class SigninComponent implements OnInit {

  //Errors
  errors: string[];

  //Content variables
  loginForm: FormGroup;
  valid = true;
  constructor(public roleService: RoleService, private router: Router, private authService: AuthService, private fb: FormBuilder) {
    this.Connection();
    this.errors = [];
   }

  ngOnInit(): void {
    this.createForm();
  }


  Connection() {//Get connection
    this.authService.getConnection()
      .subscribe(
        res => {
          //console.log(res);
          this.authService.login();
          this.router.navigate(['/object-lost'])
        },
        error => {
          this.authService.logout();
          //console.log(error); 
        })
  }

  createForm() {//Create log in form
    this.loginForm = this.fb.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  resetForm() {//Resets form
    this.loginForm.reset();
  }

  login() {//Log in
    if (!this.loginForm.invalid) {
      this.roleService.postLogin(this.loginForm.value)
        .subscribe(
          res => {
            if (res['success']) {
              this.resetForm();
              localStorage.setItem('token', res['token']);
              localStorage.setItem('role', res['role']);
              this.authService.login();
              this.authService.setRole(res['role']);

              this.router.navigate(['/object-lost']);
            } else {
              this.errors = res['errors'];
              //console.log(res);
            }
          },
          error => {

            console.log(error)
          }
        )
    }
  }


  emailVal(event) {//Validates email input
    let patt = /^([a-z]|[.-]|\d)$/;
    let result = patt.test(event.key);
    return result;
  }
}
