import { AuthService } from './../../services/auth.service';
import { Component, OnInit } from '@angular/core';
import { Role } from './../../models/role';
import { Router } from '@angular/router';
import { RoleService } from 'src/app/services/role.service';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-manager',
  templateUrl: './manager.component.html',
  styleUrls: ['./manager.component.scss']
})
export class ManagerComponent implements OnInit {
  //Passwword form
  passwordForm: FormGroup;

  //Display content variables
  role:Role;//Role (admin or moderator)
  show:boolean;

  //Erros
  errors: string[];
  success:boolean;

  constructor(public roleService: RoleService,public authService: AuthService,private router: Router, private fb: FormBuilder) {
    this.Connection();
    this.show = false;
    this.errors = [];
    this.success = false;
    this.role = new Role();
   }

  ngOnInit(): void {
    this.getRole();
    this.createForm();
  }

  Connection() {
    this.authService.getConnection()
      .subscribe(
        res => {
          //console.log(res);
          this.authService.login();
        },
        error => {
          this.authService.logout();
          //console.log(error);
          this.router.navigate(['/signin'])
        })
  }

  getRole(){//Get role information
    this.roleService.getRole()
      .subscribe(res => {
        this.role = res as Role;
        this.authService.login();
      },
      
        error => {this.authService.logout();
          this.router.navigate(['/signin'])}
      )
  }

  createForm() {//Creating password form
    this.passwordForm = this.fb.group({
      currentPassword: ['', Validators.required],
      newPassword: ['', Validators.required],
      confirmNewPassword: ['', Validators.required]
    });
  }

  get f() { return this.passwordForm.controls; }

  resetForm(form?: FormGroup) {//Reseting form
    form.reset();
    this.errors = [];
  }

  showPassword() {//Showing password
    this.show = !this.show;
  }

  changePassword(){//Change passrod to DB
    if (!this.passwordForm.invalid) {
      this.errors = [];
      this.roleService.changePassword(this.passwordForm.value)
        .subscribe(res => {
          this.success = res['success'];
          if(!this.success){
            this.errors = res['errors'];
          }
          
          //this.router.navigate(['/signin']);
        },
          error => {
            //console.log(error);
            //const {errors} = error;
            //console.log(error.data)
          }
        )
    }
  }

  closeSession(){//Close session
    this.authService.logout().subscribe();
    this.router.navigate(['/signin'])
  }
  

}
