import { AuthService } from './../../services/auth.service';
import { Role } from './../../models/role';
import { Router } from '@angular/router';
import { RoleService } from './../../services/role.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import * as $ from "jquery";
@Component({
  selector: 'app-roles',
  templateUrl: './roles.component.html',
  styleUrls: ['./roles.component.scss'],
  providers:[RoleService]
})
export class RolesComponent implements OnInit {

  //Content variables
  moderators: Role[];
  moderatorForm: FormGroup;
  selectedModeratorId: string;
  
  //Errors
  errors: string[];
  success: boolean;
  //Display variables
  showPsw: boolean;

  constructor(public roleService: RoleService,public authService: AuthService, private router: Router, private fb: FormBuilder) {
    this.Connection();
    this.errors = [];
    this.success= false;
    this.showPsw = false;
   }

  ngOnInit(): void {
    this.getModerators();
    this.createForm();
  }

  Connection() {
    this.authService.getConnection()
      .subscribe(
        res => {
          //console.log(res);
          this.authService.login();
        },
        error => {
          this.authService.logout();
          //console.log(error);
          this.router.navigate(['/signin'])
        })
  }

  getModerators(){//Get all moderators
    this.roleService.getModerators()
      .subscribe(res => {
        this.moderators = res as Role[];
        this.authService.login();
       // console.log(this.moderators)
      },
        error => this.router.navigate(['/signin'])
      )
  }

  get f() { return this.moderatorForm.controls; }


  createForm(){//Creates a form to add a moderator
      this.moderatorForm = this.fb.group({
        name: ['', Validators.required],
        lastname: ['', Validators.required],
        username:['', Validators.required],
        password: ['', [Validators.required/*, Validators.minLength(8), Validators.pattern('((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[-!@#$%^&/?]).{8,18})')*/]],
        confirmPassword: ['', Validators.required]
      });
  }

  resetForm(form?: FormGroup) {//Resets form
    form.reset();
    this.errors= [];
  }

  addModerator(){//Adds a moderator
    if (!this.moderatorForm.invalid) {
      this.roleService.postModerator(this.moderatorForm.value)
        .subscribe(res => {
          //console.log(res)
          this.success = res['success']
          if(res['errors']){
            this.errors = res['errors'];
          }else{
            this.resetForm(this.moderatorForm);
            this.getModerators();
            this.errors = [];

          }

          //this.router.navigate(['/signin']);
        },
          error => {
            //console.log(error);
            //const {errors} = error;
            //console.log(error.data)
          }
        )

    }
  }

  deleteModerator() {//Delete a moderator
    this.roleService.deleteModerator(this.selectedModeratorId)
      .subscribe(res => {
        //this.resetForm(form);
        this.getModerators();
        document.getElementById('confirmationCancel').click();
      }, error => {
        console.log("error deleting")
      })
  }

  selectModerator(id:string){
    this.selectedModeratorId = id;
  }

  showPassword() { //Show password
    this.showPsw = !this.showPsw;
  }
}
