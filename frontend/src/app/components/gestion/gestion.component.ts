import { environment } from './../../../environments/environment';
import { AuthService } from 'src/app/services/auth.service';
import { Component, OnInit } from '@angular/core';
import { ObjectService } from 'src/app/services/object.service';
import { Objet } from './../../models/object';
import { Router } from '@angular/router';

@Component({
  selector: 'app-gestion',
  templateUrl: './gestion.component.html',
  styleUrls: ['./gestion.component.scss'],
  providers: [ObjectService]
})
export class GestionComponent implements OnInit {

  //Selected object variables
  selectedObjectId: string;
  selectedType = -1;
  selectedDescription = [""]; 
  selectedDate = '1968-11-16T00:00:00'; 
  private rootAddresePhoto;
  blockUserId: string;
  blockUserUsername: string;
  blockCheckbox: boolean;
  modalPath: string;
  modalDescription: string;

  //Filter variables
  objectFilterParameter: number;  //Type of object value for filter
  dateFilterParameter: number; //Date value for filter
  descriptionFilterParameter: string;//Description value for filter
  calendarPicker: Date;
  calendar: Date;
  now: Date;
  objectDate: Date;
  dateMap: string[];
  filter_arr = [];


  //Content variables
  arrayAAfficher = []; //Array with all the objects requested
  objectsMap: string[];

  //Pagination
  loading: Boolean;
  hasPrevPage : Boolean;
  hasNext:Boolean;
  currentPage: number;
  fakeArrayTotalPages: number[];

  constructor(public objectService: ObjectService, private router: Router, public authService:AuthService ) {
    this.Connection();
    this.dateMap = environment.dateMap;
    this.rootAddresePhoto = environment.URL_API;
    this.objectsMap = environment.objectsMap;
    this.blockCheckbox = true;
    this.objectFilterParameter = -1;
    this.dateFilterParameter = -1;
    this.descriptionFilterParameter = "";
    this.calendarPicker = new Date();
    this.hasPrevPage = true;
    this.hasNext = true;
    this.currentPage = 1;
    this.fakeArrayTotalPages = [];
  }

  ngOnInit(): void {
    this.getObjects();
  }

  Connection() {
    this.authService.getConnection()
      .subscribe(
        res => {
          //console.log(res);
          this.authService.login();
        },
        error => {
          this.authService.logout();
          //console.log(error);
          this.router.navigate(['/signin'])
        })
  }

  getObjects() { //Get all the objects with or without a filter
    this.objectService.getObjectsWithUsers(this.currentPage,this.filter_arr)
      .subscribe(res => {
        this.authService.login();
        this.objectService.objects = res['docs'] as Objet[];
        this.fakeArrayTotalPages = new Array(res['totalPages']);
        this.hasPrevPage=  res['hasPrevPage'];
        this.hasNext=  res['hasNextPage'];
        this.currentPage = res['page'];
        this.arrayAAfficher = this.objectService.objects;
        this.loading = false;
        this.sendToTop();
        //console.log(this.objectService.objects);
      },
        error => this.router.navigate(['/signin'])
      )
  };

  deleteObject() { //Delete an specific object
    this.objectService.deleteObject(this.selectedObjectId)
      .subscribe(res => {
        //console.log("Object deleted")
        this.getObjects();
        document.getElementById('confirmationCancel').click();
      }, error => {
        //console.log("error deleted")
      })

  }

  getAddresIP() {//Get the photo Addresse
    return this.rootAddresePhoto;
  }

  selectObject(_id: string) { //Select an object
    this.selectedObjectId = _id;
  }

  cleanFilter() { //Resets filters
    this.dateFilterParameter = -1;
    this.descriptionFilterParameter = "";
    this.objectFilterParameter = -1;
    this.arrayAAfficher = this.objectService.objects;
  }

  filter() { //Create the filter
    if(!this.checkFilterChanges()){
      this.currentPage = 1;
    }
    this.filter_arr = [];
    if(this.objectFilterParameter != -1){
      this.filter_arr.push(['object',this.objectFilterParameter]);
    }
    if (this.dateFilterParameter != -1){
      this.filter_arr.push(['date',this.dateFilterParameter,this.calendarPicker]);
    }
    if(this.descriptionFilterParameter != ""){
      this.filter_arr.push(['description',this.descriptionFilterParameter]);
    }
    this.getObjects();
  }

  //Objet type Filter
  objectFilter = (object: any) => {
    return this.objectFilterParameter == Number(object.type);
  }


  //Description filter
  descriptionFilter = (object: any) => {
    var descriptionSplit = this.descriptionFilterParameter.split(" ");
    return descriptionSplit.every(item_str => object.description.includes(item_str));
  }

  isToday(someDate: Date) { //Checks if a given date is the same day
    const today = new Date()
    return someDate.getDate() == today.getDate() &&
      someDate.getMonth() == today.getMonth() &&
      someDate.getFullYear() == today.getFullYear()
  }

  //Set user to the top of the page
  sendToTop() {
    let scrollToTop = window.setInterval(() => {
      let pos = window.pageYOffset;
      if (pos > 0) {
        window.scrollTo(0, pos - 20); // how far to scroll on each step
      } else {
        window.clearInterval(scrollToTop);
      }
    }, 16);
  }

  //Loads the next objects page
  loadMoreObjects(page:number){
    this.currentPage = page;
    this.getObjects();
  }

  //Shows a bigger image with te full description
  previewImage(modalPath: string, description: string) {
    this.modalPath = modalPath;
    this.modalDescription = description;
  }

  //Verifies iif the wase a change in the filter
  checkFilterChanges(){
    let verificator = 0;
    this.filter_arr.forEach((filter) =>{
      if(filter[0]== "date"){
        if(filter[1] == this.descriptionFilterParameter && filter[2] == this.calendarPicker){
          verificator++;
        }
      }
      if(filter[0]== "description"){
        if(filter[1] == this.descriptionFilterParameter){
          verificator++;
        }
      }
      if(filter[0]== "object"){
        if(filter[1] == this.objectFilterParameter){
          verificator++;
        }
      }
    }); 
    return verificator == this.filter_arr.length;
  }

}
