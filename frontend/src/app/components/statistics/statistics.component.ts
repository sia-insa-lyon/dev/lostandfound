import { StatisticsService } from './../../services/statistics.service';
import { Statistics } from './../../models/statistics';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.scss']
})
export class StatisticsComponent implements OnInit {
  statistics:Statistics;
  saleData = [];
  label = "Nombre total d\'objets publiés sur le site"
  view: any[] = [1000, 300];
  animations = true;
  colorScheme = {
    domain: ['#C7B42C','#5AA454', '#A10A28', '#AAAAAA','#E133D8']
  };
  constructor(public statisticsService: StatisticsService) { 
    this.setData();
  }

  ngOnInit(): void {

  }

  
  setData(){
    this.statisticsService.getObjectStatistics()
      .subscribe(res => {
        this.statistics = res as Statistics;
        this.saleData = [
          { name: "Objets actifs", value: this.statistics.active },
          { name: "Objets retrouvés", value: this.statistics.found },
         // { name: "Objets suprimmés par les utilisateurs", value: this.statistics.deleted },
          { name: "Objets expirés", value: this.statistics.expired},
          { name: "Objets suprimmés par les administrateurs/modérateurs", value: this.statistics.reported}
        ];
      },
        error => console.log(error)
      )
  };

}
