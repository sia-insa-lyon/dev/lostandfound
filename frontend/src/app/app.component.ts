import { ObjectService } from './services/object.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from './services/auth.service';
import { Router } from '@angular/router';
import { Component } from '@angular/core';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {
  title = 'Lost and found';
  logged = false;//a cambiar
  codeForm: FormGroup;
  errors:string[];
  success: boolean;

  constructor(public authService:AuthService, private router: Router, private fb: FormBuilder,private objectService:ObjectService) {
    this.createCodeForm();
    this.errors = [];
    this.success = false;
  }
  createCodeForm() {
    this.codeForm = this.fb.group({
      code: ['', [Validators.required, Validators.pattern("[0-9A-Za-z]{8}")]],
    });
  }

  logout() {
    this.authService.logout()
      .subscribe(
        data => {
          //console.log(data);
          this.router.navigate(['/signin'])
        },
        error => console.error(error)
      )
  }

  sendCode(){//Declare an object found by his owner
    this.objectService.objectFound(this.codeForm.controls['code'].value)
    .subscribe(res => {
      this.success = res['success'];
      if(this.success){
        document.getElementById('cancelCodeModal').click();
        this.resetForm();
      }else{
        this.errors= res['errors'];
        
      }
      //console.log(res);
    }, error => {
      console.log("error found")
    })
  }

  getMessage(res:string) {
    return res;
  }

  resetForm(){
    this.codeForm.reset();
  }



}
