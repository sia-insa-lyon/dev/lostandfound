import { NotFoundComponent } from './components/not-found/not-found.component';
import { TokenInterceptorService } from './services/token-interceptor.service';

import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms'
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ObjectFoundComponent } from './components/object-found/object-found.component';
import { ObjectLostComponent } from './components/object-lost/object-lost.component';

import { SigninComponent } from './components/signin/signin.component';

import { AgmCoreModule } from '@agm/core';
import { ReactiveFormsModule } from '@angular/forms';
//CHART
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

//CAPTCHA
import { RecaptchaModule, RecaptchaFormsModule } from 'ng-recaptcha';

//FRENCH DATES
import { LOCALE_ID } from '@angular/core';
import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
registerLocaleData(localeFr);

//NUMBER INPUT
import { Ng2TelInputModule } from 'ng2-tel-input';

//Leaflet Map
import { LeafletModule } from '@asymmetrik/ngx-leaflet';

//Image compress
import { NgxImageCompressService } from 'ngx-image-compress';
import { ManagerComponent } from './components/manager/manager.component';
import { GestionComponent } from './components/gestion/gestion.component';
import { StatisticsComponent } from './components/statistics/statistics.component';
import { RolesComponent } from './components/roles/roles.component';
import { AboutComponent } from './components/about/about.component';
import { TestComponent } from './components/test/test.component';
import { CovidComponent } from './components/covid/covid.component';



@NgModule({
  declarations: [
    AppComponent,
    ObjectFoundComponent,
    ObjectLostComponent,
    SigninComponent,
    ManagerComponent,
    GestionComponent,
    StatisticsComponent,
    RolesComponent,
    AboutComponent,
    TestComponent,
    NotFoundComponent,
    CovidComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    Ng2TelInputModule,
    BrowserAnimationsModule,
    NgxChartsModule,
    LeafletModule,
    RecaptchaModule,  //this is the recaptcha main module
    RecaptchaFormsModule, //this is the module for form incase form validation
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDAH49QrWwcdWw7aDCbLWrN_g__jYcniFk'
    })
  ],
  providers: [{ provide: LOCALE_ID, useValue: "fr-CA" }, NgxImageCompressService,
  {
    provide: HTTP_INTERCEPTORS,
    useClass: TokenInterceptorService,
    multi: true
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
