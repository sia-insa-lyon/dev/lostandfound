export class Role {
    constructor (id='',name='',lastname='',username='',date='1968-11-16T00:00:00',role='',password=''){
        this._id=id;
        this.name=name;
        this.lastname=lastname;
        this.username= username;
        this.role = role;
        this.password=password;
        this.date=new Date(date);
    }
    _id:string;
    name:string;
    lastname:string;
    username:string;
    role:string;
    password:string;
    date:Date;

}

