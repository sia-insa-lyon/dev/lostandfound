export class Objet {
    constructor (id='',type='',latitude=-1,longitude=-1,description='',imagePath='',foundcode='',date='1968-11-16T00:00:00',name='',email=''){
        this._id=id;
        this.type=type;
        this.latitude= latitude;
        this.longitude= longitude;
        this.description = description;
        this.imagePath = imagePath;
        this.foundcode = foundcode;
        this.name=name;
        this.email= email;
        this.date=new Date(date);
    }
    _id:string;
    type:string;
    latitude:number;
    longitude:number;
    description:string;
    imagePath:string;
    foundcode: string;
    name:string;
    email:string;
    date: Date;

}
