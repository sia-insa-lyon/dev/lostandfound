export class Statistics {
    constructor (id = '', entity = '', active = 0 , found = 0, reported = 0 , expired = 0){
        this._id = id;
        this.entity = entity;
        this.active = active;
        this.found = found;

        this.reported = reported;
        this.expired = expired ;
    }
    _id:string;
    entity:string;
    active:Number;
    found:Number;

    reported:Number;
    expired:Number;
}
