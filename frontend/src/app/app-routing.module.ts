import { AboutComponent } from './components/about/about.component';
import { CovidComponent } from './components/covid/covid.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { TestComponent } from './components/test/test.component';
import { AuthGuard } from './guards/auth.guard';
import { RolesComponent } from './components/roles/roles.component';

import { StatisticsComponent } from './components/statistics/statistics.component';
import { GestionComponent } from './components/gestion/gestion.component';
import { ManagerComponent } from './components/manager/manager.component';

import { SigninComponent } from './components/signin/signin.component';
import { ObjectLostComponent } from './components/object-lost/object-lost.component';
import { ObjectFoundComponent } from './components/object-found/object-found.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';



// data : PERSONS HAVING ACCESS TO THE SITE
//NONE = EVERYBODY
const routes: Routes = [
  { path: '', redirectTo: 'object-lost', pathMatch: 'full' },
  {
    path: 'management', component: ManagerComponent,
    data: {
      role: ['ADMIN', 'MODERATOR']//Entity having access to the routes
    },
    canActivate: [AuthGuard],
    children: [
      {
        path: 'roles', component: RolesComponent,
        data: {
          role: ['ADMIN']
        },
        canActivate: [AuthGuard],
      },
      {
        path: 'administration', component: GestionComponent,
        data: {
          role: ['MODERATOR', 'ADMIN']
        },
        canActivate: [AuthGuard],
      },
      {
        path: 'statistics', component: StatisticsComponent,
        data: {
          role: ['MODERATOR', 'ADMIN']
        },
        canActivate: [AuthGuard]
      }
    ]
  }
  ,
  {
    path: 'object-found',
    component: ObjectFoundComponent,
    canActivate: [AuthGuard],
    data: {
      role: ['NONE']
    }
  },
  {
    path: 'signin', component: SigninComponent,
    canActivate: [AuthGuard],
    data: {
      role: ['NONE']
    }
  },
  {
    path: 'object-lost', component: ObjectLostComponent,
    canActivate: [AuthGuard],
    data: {
      role: ['NONE']
    }
  }, /*{
    path: 'test', component: TestComponent,
    data: {
      role: ['NONE']
    }
  },*/{
    path: 'covid', component: CovidComponent, 
    data: {
      role: ['NONE']
    }
  },{
    path: 'about', component: AboutComponent, 
    data: {
      role: ['NONE']
    }
  }
  
  , {
    path: '404', component: NotFoundComponent, 
    canActivate: [AuthGuard], 
    data: {
      role: ['NONE']
    }
  },
  {
    path: '**', redirectTo: '404', 
    canActivate: [AuthGuard], 
    data: {
      role: ['NONE']
    }
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
