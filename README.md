# Lost n' Found

## Overview

This app unifies the way to put in contact someone that has lost an object on the campus of INSA de Lyon and the person who found it. 

## Features

* Any person can post an ad of the found object, describing the type of the object, where it has been found, a description and a photo. This person must add enter an email to get in contact by the owner.
* If the owner identifies his object, he will be able to add an email and/or a telephone number to be in contact with the person who created the ad.
* Once the object is with his owner, it can be declared as "Found by his owner" and all its data will be deleted. If that is not the case, the object will be deleted automatically in an defined interval of time.
* Administrators and moderators manage all the content posted on the site.


## Deployement

### Backend Environement Variables

On backend file (LostAndFound/backend/.env) set ADMIN variables to create a master profile in the database. Then the values must be deleted.
```
ADMIN_NAME = "MY_NAME"
ADMIN_LASTNAME = "MY_LASTNAME"
ADMIN_USERNAME = "test_admin"
ADMIN_PASSWORD = "12345"
```
Then you will be able to access as Administrator or Manager on 
```
http://localhost:3000/signup
```

Set all keys and connection variables to deploy the server.

### Frontend Environement Variables

On frontend (LostAndFound/frontend/scr/environments/environment.ts) set the next connection and dependencies variables.
```
  URL_API : 'http://localhost:3000/', 
  CAPTCHA_WEB_KEY:'6Lf6k-AZAAAAALaawD6nn-UdMfN5OKRgwCOFYCQQ',
```



### Create an image of the project
Run in a terminal located on /LostAndFound (where .yml file is located)
```bash
docker-compose build
```

### Run the image of the project

```bash
docker-compose up
```
## Dependencies
  * Angular
  * NodeJS
  * Express
  * MongoDB
  * Docker
  * Boostrap
  * OpenStreetMap
  * Nodemailer

## Contributing
[IBAÑEZ CELIS Israel](https://www.linkedin.com/in/israel-ibanez-celis/)

SIA INSA de Lyon


## License
Lost N' Found is licensed under the GNU GPL v3.


Copyright © 2020 Ibañez Celis Israel

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.


You should have received a copy of the GNU General Public License
along with this program.  If not, see http://www.gnu.org/licenses/.