const Statistics = require('../models/Statistics');
const statisticsCtrl = {};
const path = require('path');


//Get statistiques
statisticsCtrl.getObjectStatistics = async (req, res) => {
    const statistics = await Statistics.findOne({entity:'Objet'})
    res.status(200).json(statistics);
};


module.exports = statisticsCtrl;
