const Objet = require('../models/Object');
const Statistics = require('../models/Statistics');
const objectCtrl = {};
const path = require('path');
const date_fns = require('date-fns');//Date formatting
const fs = require('fs-extra');//Accessing to photos
const cron = require('node-cron');//Cron function (function executed each period time)
const emailCtrl = require('./../config/mail.js');


/********************** OBJECT CRUD***************** */

//Object creation
objectCtrl.createObject = async (req, res) => {
    const { type, latitude, longitude, description, name, email, confirmEmail } = req.body;

    const errors = [];
    let stringRegex = /[0-9\@\_\.\*\-\$\!\%\#\+\t]+/;
    let emailRegex = /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/;

    let floatRegex = /^[0-9.]{2,20}$/;
    var imagePath;
    var objectArray = process.env.OBJECT_ARRAY.split(',');

    //console.log(req.body)
    if (req.invalidCaptcha) {
        errors.push({ text: 'Recaptcha pas valide' });
    }

    if (!type) {
        errors.push({ text: "Inserez un type d\'objet" });
    } else if (isNaN(type) || (type <= 0 || type > objectArray.length)) {
        errors.push({ text: 'Inserez un type d\'objet valide' });
    }

    if (!name) {
        errors.push({ text: "Inserez un nom" });
    } else if (name.length <= 0) {
        errors.push({ text: 'Inserez nom nom' });
    } else if (name.length > 25) {
        errors.push({ text: 'Inserez un nom valide' });
    } else if (stringRegex.test(name)) {
        errors.push({ text: 'Insérez un nom valide' });
    }

    if (!email) {
        errors.push({ text: "Inserez une adresse mail" });
    } else if (!emailRegex.test(email)) {
        errors.push({ text: 'Insérez un mail valide' });
    }

    if (!confirmEmail) {
        errors.push({ text: 'Confirmez votre adresse mail' });
    } else if (email != confirmEmail) {
        errors.push({ text: 'Les adresses mails ne conïncident pas' });
    }

    if (!latitude || !longitude) {
        errors.push({ text: 'Inserez une localisation' });
    } else if (!floatRegex.test(latitude) || !floatRegex.test(longitude)) {
        errors.push({ text: 'Insérez des coordonnées valides' });
    }

    if (!description) {
        errors.push({ text: "Inserez une description" });
    } else if (description.length <= 0) {
        errors.push({ text: "veuillez entrer une description pour votre objet!" });
    } else if (description.length > 150) {
        errors.push({ text: "Veuillez entrer une description plus courte" });
    }



    if (req.fileValidationError) {
        errors.push({ text: req.fileValidationError });
    }
    imagePath = false;//Object without photo

    if (errors.length > 0) {
        res.status(200).json({ succes: false, message: 'Error adding an object', errors: errors });
    } else {

        const foundcode = await getRandomFoundcode(process.env.FOUNDCODE_LENGTH);
        const newObject = new Objet({ type, latitude, longitude, description, name, email, confirmEmail, foundcode: foundcode, imagePath: imagePath });

        //console.log(newObject);
        await newObject.save();//Asynchronus process, to wait until the task be finished
        // await emailCtrl.objectPosted({ receiver: email, type: type, name: name, foundcode: foundcode, date: newObject.date });//Mail confirmation
        //Satistics update
        const statistics = await Statistics.findOne({ entity: 'Objet' });
        if (!statistics) {
            //console.log("First time")
            const newStatistics = Statistics({ entity: 'Objet', active: 1, found: 0, deleted: 0, reported: 0, expired: 0 })
            await newStatistics.save();
        } else {
            await Statistics.findOneAndUpdate({ entity: 'Objet' }, { active: statistics.active + 1 });
        }
        //console.log(newObject);
        res.status(200).json({ success: true, message: 'Saved' });
    }

};

//Get random code
async function getRandomFoundcode(length) {
    var randomChars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var result = '';
    var object;
    do {
        for (var i = 0; i < length; i++) {
            result += randomChars.charAt(Math.floor(Math.random() * randomChars.length));
        }
        object = await Objet.findOneAndDelete({ foundcode: result });
        if (object) {
            result = '';
        }
    } while (object);
    return result;
}

//Get all the objects
objectCtrl.getObjects = async (req, res) => {
    const { page } = req.params || 1;
    let { filter } = req.body;
    const filterToMongo = "{" + createFilter(filter) + "}";
    objects = await Objet.paginate(JSON.parse(filterToMongo), { limit: process.env.OBJECTS_SHOWN_BY_PAGE, page: page, sort: { date: -1 } });
    res.status(200).json(objects);
};




//Delete an object 
objectCtrl.deleteObject = async (req, res) => {
    //console.log(req.user.role);
    const objet = await Objet.findByIdAndDelete(req.params.id);
    if (objet.imagePath != "false") {
        await fs.unlink(path.resolve(objet.imagePath));//Delete image
    }
    const statistics = await Statistics.findOne({ entity: 'Objet' });
    if (req.user.role == 'USER') { //Update statictics
        await Statistics.findOneAndUpdate({ entity: 'Objet' }, { deleted: statistics.deleted + 1, active: statistics.active - 1 });//Object deleted by an USER
    } else {
        await Statistics.findOneAndUpdate({ entity: 'Objet' }, { reported: statistics.reported + 1, active: statistics.active - 1 });//Object deleted by a MANAGER
    }
    res.status(200).json({ succes: true, message: "Object deleted" });
};



/**********************  OBJECT CRUD END***************** */


//Filter creation
function createFilter(filter) {//Createa search filter to objects. Receiving a date or a type of object or a description
    let filterToMongoose = "";
    let i = 0;
    filter.forEach(filterFunction => {
        if (filterFunction[0] == "object") {
            filterToMongoose += "\"type\": \"" + filterFunction[1] + "\" "
        }
        if (filterFunction[0] == "date") {
            filterToMongoose += "\"date\": {" + dateFilter(filterFunction[1], filterFunction[2]) + "} "
        }
        if (filterFunction[0] == "description") {
            filterToMongoose += "\"description\": { \"$regex\": \"" + filterFunction[1] + "\", \"$options\": \"i\" } "
        }
        i += 1;
        if (i != filter.length) {
            filterToMongoose += ","
        }
    });
    return filterToMongoose;
}


//Date filter creation
function dateFilter(option, date) {
    dateFilterDB = ""
    date = new Date(new Date(date).getFullYear(), new Date(date).getMonth(), new Date(date).getDate());
    switch (parseInt(option)) {
        case 0://Today       
            dateFilterDB = "\"$gte\": \"" + date_fns.startOfDay(new Date()) + "\",\"$lte\": \"" + date_fns.endOfDay(new Date()) + "\"";
            break;
        case 1:// Yesterday
            dateFilterDB = "\"$gte\":\"" + date_fns.startOfDay(date_fns.sub(new Date(), { days: 1 })) + "\",\"$lte\": \"" + date_fns.endOfDay(date_fns.sub(new Date(), { days: 1 })) + "\"";
            break;
        case 2:// 3 last days
            dateFilterDB = "\"$gte\":\"" + date_fns.startOfDay(date_fns.sub(new Date(), { days: 2 })) + "\",\"$lte\": \"" + date_fns.endOfDay(new Date()) + "\"";
            break;
        case 3: //Last week
            dateFilterDB = "\"$gte\":\"" + date_fns.startOfDay(date_fns.sub(new Date(), { days: 6 })) + "\",\"$lte\": \"" + date_fns.endOfDay(new Date()) + "\"";
            break;
        case 4: //Special day
            dateFilterDB = "\"$gte\":\"" + date_fns.startOfDay(date) + "\",\"$lte\": \"" + date_fns.endOfDay(date) + "\"";
            break;
        case 5: //After
            dateFilterDB = "\"$gte\": \"" + date_fns.startOfDay(date) + "\"";
            break;
        case 6: //Before
            dateFilterDB = "\"$lte\": \"" + date_fns.endOfDay(date) + "\"";
            break;
    }
    return dateFilterDB;
}


//Delete from the system all the expired objects after a time designed
cron.schedule('*/' + process.env.CHECKING_EXPIRED_OBJECT_TIME_IN_MINUTES + ' * * * *', async () => {
    const scaleTime = process.env.TIME_SCALE;// days |  hours  | minutes
    const quantity = process.env.TIME_BEFORE_EXPIRATION;
    console.log('Deleting expired object after: ' + quantity + ' ' + scaleTime);
    const timeExpiration = JSON.parse("{\"" + scaleTime + "\":" + quantity + "}")
    const expiredObjectImageRoutes = await Objet.find({ date: { $lte: date_fns.sub(new Date(), timeExpiration) } }, { "imagePath": 1 });
    await Objet.deleteMany({ date: { $lte: date_fns.sub(new Date(), timeExpiration) } }, (err) => {
        if (err) {
            console.log(err);
        }
    });
    for (const imageRoute of expiredObjectImageRoutes) {
        if (imageRoute.imagePath != "false") {
            await fs.unlink(path.resolve(imageRoute.imagePath));//Delete image
        }
    }
    if (expiredObjectImageRoutes.length > 0) {
        console.log("Deleted items: " + expiredObjectImageRoutes.length)
        const statistics = await Statistics.findOne({ entity: 'Objet' });
        await Statistics.findOneAndUpdate({ entity: 'Objet' }, { expired: statistics.expired + expiredObjectImageRoutes.length, active: statistics.active - expiredObjectImageRoutes.length });
    }
});

//Delete from the system an object after beeing declared as found
objectCtrl.objectFound = async (req, res) => {
    const foundcode = req.params.id;
    if (foundcode) {
        var foundcodeRegex = /^[a-zA-Z0-9]*$/;
        if (foundcode.length == process.env.FOUNDCODE_LENGTH && foundcodeRegex.test(foundcode)) {
            const objet = await Objet.findOneAndDelete({ foundcode: foundcode });
            if (objet) {
                if (objet.imagePath != "false") {
                    await fs.unlink(path.resolve(objet.imagePath));//Delete image
                }
                const statistics = await Statistics.findOneAndUpdate({ entity: 'Objet' });
                await Statistics.findOneAndUpdate({ entity: 'Objet' }, { active: statistics.active - 1, found: statistics.found + 1 });
                res.status(200).json({ success: true, message: 'Object found by the owner!' });
            } else {
                res.status(200).json({ success: false, message: 'Invalid code', errors: [{ text: "Code d\'objet inexistant!" }] });
            }
        } else {
            res.status(200).json({ success: false, message: 'Invalid code', errors: [{ text: "Code pas valide" }] });
        }
    } else {
        res.status(200).json({ success: false, message: 'Invalid code', errors: [{ text: "Inserez un code" }] });
    }

}


objectCtrl.contactPerson = async (req, res) => {
    const { email, confirmEmail, number, dial } = req.body;
    const errors = [];
    let numberRegex = /^[0-9]{8,12}$/;
    let emailRegex = /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/;



    if (req.invalidCaptcha) {
        errors.push({ text: req.invalidCaptcha });
    }

    if (!email) {
        errors.push({ text: "Inserez une adresse mail" });
    } else if (!emailRegex.test(email)) {
        errors.push({ text: 'Insérez un mail valide' });
    }

    if (!confirmEmail) {
        errors.push({ text: 'Confirmez votre adresse mail' });
    } else if (email != confirmEmail) {
        errors.push({ text: 'Les adresses mails ne conïncident pas' });
    }

    if (number) {
        if (!numberRegex.test(number)) {
            errors.push({ text: "Inserez un numéro de téléphone valide" });
        }
    }
    const objet = await Objet.findById(req.params.id);
    if (errors.length == 0) {
        if (objet) {
            //console.log(req.body);
            emailCtrl.MailToPossibleOwner({ owner: email, name: objet.name, finder: objet.email });
            emailCtrl.MailToFinder({ owner: email, name: objet.name, finder: objet.email, number: number, dial: dial });
            res.status(200).json({ success: true, message: 'Emails sent' });
        } else {
            res.status(200).json({ success: false, message: 'Unfound object', errors: { text: 'Objet inexistant' } });
        }
    } else {
        res.status(200).json({ success: false, message: 'Form errors', errors: errors });
    }
    //console.log(objet);
}

/************************************************TESTS************************************************* */


//Photos testing
objectCtrl.createPhoto = async (req, res) => {
    //console.log(req.file.size / 1000000);
    if (req.file.size / 1000000 > process.env.IMAGE_SIZE_MB_LIMIT) {
        return res.status(413).json({ success: false, message: 'Photo size too large! 5Mo max.' })
    }
    res.status(200).json({ success: true, message: 'Photo successfully saved!' })
}

objectCtrl.getObjectsFilter = async (req, res) => {
    let { type, description } = req.params;
    console.log(description);
    const filtro = "{\"type\":\"" + type + "\",\"date\":{}}";
    let filtroJSON = JSON.parse(filtro);
    filtroJSON.date = "{\"$gte\":\"" + date_fns.startOfDay(new Date()) + "\",\"$lte\": \"" + date_fns.endOfDay(new Date()) + "\"}";
    console.log(filtroJSON)
    const objects = await Objet.paginate({}, { limit: 4, sort: { date: -1 } });
    res.status(200).json(objects);
}

objectCtrl.getPhotos = async (req, res) => {
    const objects = await Objet.find().sort({ date: 'desc' });
    res.status(200).json(objects);

}

objectCtrl.getUserObjectsTest = async (req, res) => {
    const objects = await Objet.find({ user: req.params.id }).sort({ date: 'desc' });
    console.log(req.params.id);
    //console.log(objects);
    res.status(200).json(objects);
};




/************************TESTS END********************* */



module.exports = objectCtrl;