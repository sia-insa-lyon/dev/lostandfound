const Role = require('../models/Role');
const roleCtrl = {};
const jwt = require('jsonwebtoken')
//const mail = require('./../config/mail.js')

roleCtrl.getAccess = (req, res) => {
    const token = jwt.sign({_id: req.user._id,role:req.user.role}, process.env.AUTHO_TOKEN_SECRET_KEY, {expiresIn:'24h'});
        res.status(200).json({ success: true, message: 'Logged',token:token });
    
}

/****************************************** ROLE CRUD ************************************** */

//FIRST ADMIN 
roleCtrl.createAdmin =async(req, res)=>{
    const admin = await Role.find({role:"ADMIN"});
    const name = process.env.ADMIN_NAME;
    const lastname = process.env.ADMIN_LASTNAME;
    const username = process.env.ADMIN_USERNAME;
    const password = process.env.ADMIN_PASSWORD;
    if(admin.length == 0){
        const newRole = new Role({ name:name, lastname:lastname, username:username, password:password, role:'ADMIN' });
        newRole.password = await newRole.encryptPassword(password);
        await newRole.save();
        console.log('ADMIN SAVED as: ' + username);
    }else{
        console.log('ADMIN ALREADY REGISTERED')
    }
    console.log('Server connected on '+process.env.SERVER_ADDRESSE + ' :',process.env.PORT);
}

//ROLE CREATION (ADMIN OR MODERATOR)
roleCtrl.createRole = async (req, res) => {
   // console.log(req.body);
    const { name, lastname, username, role, password, confirmPassword } = req.body;
    const errors = [];
    if (!name) {
        errors.push({ text: "Le champ nom est vide, complétez-le!" });
    } else if (name.length <= 0) {
        errors.push({ text: 'Inserez votre prénom' });
    }

    if (!lastname) {
        errors.push({ text: "le champ nom est vide, complétez-le!" });
    } else if (lastname.length <= 0) {
        errors.push({ text: 'Inserez votre nom' });
    }

    if (!username) {
        errors.push({ text: "le champ email est vide, complétez-le!" });
    } else if (username.length <= 0) {
        errors.push({ text: "Inserez le début de votre mail de l'INSA" });
    } else if (username.includes("@")) {
        errors.push({ text: 'Ne mettez pas l\'extension de l\'adresse mail' });
    }


    if (!password) {
        errors.push({ text: "Le champ mot de passe est resté vide, complétez-le!" });
    } else if (password.length <= 0) {
        errors.push({ text: 'Inserez un mot de passe' });
    } else if (password.length < 4) {
        errors.push({ text: "Le mot de passe doit être composé d'au moins 4 lettres" });
    }
    if (!confirmPassword) {
        errors.push({ text: "Vous devez confirmer votre mot de passe!" });
    } else if (confirmPassword.length <= 0) {
        errors.push({ text: 'Confirmez votre mot de passe' });
    } else if (password != confirmPassword) {
        errors.push({ text: 'Les mots de passe ne correspondent pas' });
    }

    if (errors.length > 0) {
        res.status(200).json({ success: false, message: errors });
    } else {
        const usernameRole = await Role.findOne({ username: username });
        if (usernameRole) {
            errors.push({ text: "Username already in use" });
            //req.flash('error_msg','The email is already in use');
            res.status(200).send({ success: false, message: 'Nom d\'utilisateur déjà existant' });
        } else {
            const newRole = new Role({ name, lastname, username, password, role });//Cambiar a false
            newRole.password = await newRole.encryptPassword(password);
            await newRole.save();
            res.status(201).json({ success: true, message: 'Signed up' });
        }
    }
};

//ADDING MODERATOR
roleCtrl.addModerator = async (req, res) => {
    //console.log(req.body);
    const { name, lastname, username, password, confirmPassword } = req.body;
    const errors = [];
    let stringRegex = /[0-9\@\_\.\*\-\$\!\%\#\+\s\t]+/ ;
    let usernameRegex = /^[a-zA-Z0-9_]*$/;
    let passwordRegex = /^(?=.*\d)(?=.*[a-z])(?=.*[#%\\.@\\+*\-\\_])(?=.*[A-Z])(?=.*[a-zA-Z]).{6,16}$/

    if (!name) {
        errors.push({ text: "Le champ nom est vide, complétez-le!" });
    } else if (stringRegex.test(name) ) {
        errors.push({ text: 'Insérez un prénom valide' });
    }

    if (!lastname) {
        errors.push({ text: "le champ nom est vide, complétez-le!" });
    } else if (stringRegex.test(lastname) ) {
        errors.push({ text: 'Insérez un nom valide' });
    }

    if (!username) {
        errors.push({ text: "le champ email est vide, complétez-le!" });
    } else if (username.length <= 8 || username.length > 16) {
        errors.push({ text: "Inserez un identifiant entre 8 et 16 charactères " });
    }else if (!usernameRegex.test(username) ) {
        errors.push({ text: 'Insérez un identifiant valide' });
    }

    if (!password) {
        errors.push({ text: "Le champ mot de passe est resté vide, complétez-le!" });
    } else if (password.length <= 0) {
        errors.push({ text: 'Inserez un mot de passe' });
    } else if (password.length < 6 || password.length > 16) {
        errors.push({ text: "Le mot de passe doit être entre 6 et 16 charactères" });
    }else if (!passwordRegex.test(password)) {
        errors.push({ text: "Le mot de passe doit contenir au moins: une mayuscule, une minuscule, un chiffre et un charactère spécial" });
    }

    if (!confirmPassword) {
        errors.push({ text: "Vous devez confirmer votre mot de passe!" });
    } else if (confirmPassword.length <= 0) {
        errors.push({ text: 'Confirmez votre mot de passe' });
    } else if (password != confirmPassword) {
        errors.push({ text: 'Les mots de passe ne correspondent pas' });
    }

    if (errors.length > 0) {
        res.status(200).json({ success: false, message:"Error adding a moderator", errors: errors });
    } else {
        const usernameRole = await Role.findOne({ username: username });
        if (usernameRole) {
            errors.push({ text: "Nom d\'utilisateur déjà existant" });
            res.status(200).send({ success: false, message:"Error adding a moderator", errors: errors });
        } else {
            const newRole = new Role({ name, lastname, username, password, role: 'MODERATOR' });//Cambiar a false
            newRole.password = await newRole.encryptPassword(password);
            await newRole.save();
            res.status(201).json({ success: true, message: 'Modérateur enregistré' });
        }
    }
}

//GET ALL MODERATORS
roleCtrl.getModerators = async (req, res) => {
    const modarators = await Role.find({}, { "password": 0 }).sort({ username: 'desc' });
    res.status(200).json(modarators);
};

//GET SPECIFIC MODERATOR
roleCtrl.getRole = async () => {
    const role = await Role.findById(req.params.id);
    res.status(200).json(role);
}


//DELETE MODERATOR
roleCtrl.deleteModerator = async (req, res) => {
    await Role.findByIdAndDelete(req.params.id);
    res.status(200).json({ succes: true, message: 'Moderator deleted' });
}

//CHANGE ROLE (ADMIN OR MODERATOR) PASSWORD
roleCtrl.changePassword = async (req, res) => {
    const { currentPassword, newPassword, confirmNewPassword } = req.body;
    var errors = [];
    let passwordRegex = /^(?=.*\d)(?=.*[a-z])(?=.*[#%\\.@\\+*\-\\_])(?=.*[A-Z])(?=.*[a-zA-Z]).{6,16}$/

    if(!currentPassword){
        errors.push('Insérez le mot de courant ');
    }

    if(!newPassword){
        errors.push('Insérez un nouveau mot de passe ');
    }else if(newPassword.length < 6 || newPassword.length > 16){
        errors.push({ text: 'Le mot de passe doit être composé entre 6 et 16 caractères' });
    }else if (!passwordRegex.test(newPassword)) {
        errors.push({ text: 'Le mot de passe doit contenir au moins: une mayuscule, une minuscule, un chiffre et un charactère spécial (#%.@+*\-_)' });
    }
    if(!confirmNewPassword){
        errors.push({ text: 'Confirmez le nouveau mot de passe'});
    }
    if(errors.length == 0){
        if (currentPassword !=newPassword) {
            if (newPassword == confirmNewPassword) {
                const userDB = await Role.findById({ _id: req.user._id });
                const matchRole = await userDB.matchPassword(currentPassword);
                if (matchRole) {
                    const encryptedPassword = await userDB.encryptPassword(newPassword);
                    //const nose = await Role.findByIdAndUpdate(req.user._id , { password: encryptedPassword })
                    res.status(200).json({ success: true, message:'Password changed'});
                } else {
                    res.status(200).json({ success: false, message:'Error resenting password ',errors: [{text:"Mauvais mot de passe actuel"}] })
                }
            } else {
                res.status(200).json({ success: false,message:'Error resenting password ',errors: [{text:"Les 2 mots de passe ne correspendent pas" }]})
            }
        } else {
            res.status(200).json({ success: false, message:'Error resenting password ',errors: [{text:"Le mot de passe actuel et le nouveau mot de passe sont égaux"}] })
        }
    }else{
        res.status(200).json({ success: false, message:'Error resenting password ',errors:  errors })
    }
}

module.exports = roleCtrl;