const express = require('express');
const router = express.Router();
const { isAuthenticated, verifyToken, isAdmin,isModerator, isManager } = require('../helpers/auth');
const role = require('../controllers/role.controller');
const passport = require('passport');
const jwt = require('jsonwebtoken')

router.post('/signin',function (req, res, next) {
  passport.authenticate('local', function (err, user, info) {
    if (err) { return res.json({ success: false,message:'Error logging in',errors:error}); }
    if (!user) { return res.json({ success: false,message:'Error logging in',errors:info.message}); }
    req.logIn(user, function (err) {
      if (err) { return res.json({ success: false,message:err,errors:err}); }
      const token = jwt.sign({_id: user._id,role:user.role}, process.env.AUTHO_TOKEN_SECRET_KEY, {expiresIn:'24h'});//We create a token with an id and the role
      return res.json({ success: true,message:'Login success' ,token:token,role:user.role});
    });
  })(req, res, next);
});


 //Routes called anybody LOGGED

router.get('/access',isAuthenticated,verifyToken,role.getAccess);

//Route called by MANAGER (ADMIN and MODERATOR)

router.get('/role',[isAuthenticated,verifyToken, isManager],function(req,res,next){
  return res.status(201).json(req.user);
});

router.put('/roles/changePassword',[isAuthenticated,verifyToken, isManager],role.changePassword);


// Route called by ADMIN

router.post('/roles',[isAuthenticated,verifyToken, isAdmin],role.createRole);

router.get('/roles',[isAuthenticated,verifyToken, isAdmin],role.getModerators);

router.post('/roles/add',[isAuthenticated,verifyToken, isAdmin], role.addModerator);

router.delete('/roles/delete/:id',[isAuthenticated,verifyToken, isAdmin] ,role.deleteModerator);



module.exports = router;


