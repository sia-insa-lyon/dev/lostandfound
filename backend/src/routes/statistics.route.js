const express = require('express');
const router = express.Router();
const {isAuthenticated,verifyToken, isManager  } = require('../helpers/auth');
const statistics = require('../controllers/statistics.controller');


//Route called by MANAGER (ADMIN AND MODERATOR)
router.get('/statistics',[isAuthenticated,verifyToken,isManager],statistics.getObjectStatistics);

module.exports = router;