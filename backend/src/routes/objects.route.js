const express = require('express');
const router = express.Router();
const {isAuthenticated,verifyToken,verifyCaptcha,isUser,isManager,isAdmin,isModerator } = require('../helpers/auth');
const multer = require('../config/multer.js')
const object = require('../controllers/object.controller');

//Routes called by anyone LOGGED

router.put('/object-lost/:page',/*[isAuthenticated,verifyToken],*/object.getObjects);

router.get('/paginate',object.getObjects);

//Routes called by USER

router.post('/object-found/:recaptcha',[verifyCaptcha,multer.single('image')],object.createObject);


router.delete('/object/found/:id',object.objectFound);

router.delete('/user/objects/:id',[isAuthenticated,verifyToken],object.deleteObject);

router.put('/contact/:id',[verifyCaptcha],object.contactPerson);




/*********************************************TESTS************************************/

router.get('/filter/test/:type/:description',object.getObjectsFilter);

router.get('/home/test/:id',object.getUserObjectsTest);

///////PHOTOS TESTING
/*router.post('/photos'/*,object.validatePhotoSize,multer.single('image'),object.createPhoto);

router.get('/photos',object.getPhotos);

router.delete('/photos/:id',object.deleteObject)*/


////Statistics TESTING

//router.get('/statistics',object.modifyStatistics);


/*********************************************TESTS END************************************/

module.exports = router;