const express = require('express');
const path = require('path');
const methodeOverride = require('method-override');//
const session = require('express-session');
const passport = require('passport');
const morgan = require ('morgan');
const cors = require('cors');

require('dotenv').config(); //Find environementa


//Initializations
const app = express();
// MongoDB Connection
const mongoose = require('mongoose');
mongoose.connect(process.env.MONGO_DB,{
    useCreateIndex:true,
    useNewUrlParser:true,
    useFindAndModify:false,
        useUnifiedTopology:true
})
    .then(db=> console.log('DB is connected'))
    .catch(err => console.error(err));


//Settings
app.set('port',process.env.PORT || 3000);


//Middlewares
app.use(morgan('dev'));
app.use(express.urlencoded({extended:false}));
app.use(methodeOverride('_method'));//Accepts all the methods as delete,put, etc
app.use(cors({
    origin:process.env.ANGULAR_ORIGIN,
    credentials: true
}));

app.use(session({
    name:process.env.COOKIES_NAME,
    resave:false,
    saveUninitialized:false,
    secret:process.env.SESSION_SECRET_KEY,
    cookie:{
      maxAge: 36000000,
      httpOnly:false,
      secure:false
}/*,store: new MongoStore({mongooseConnection: mongoose.connection})*/
}));
require('./config/passport');
app.use(express.json());
app.use(passport.initialize());
app.use(passport.session());

//Global variables
app.use((req,res,next)=>{
    /*res.locals.success_msg = req.flash('success_msg');
    res.locals.error_msg = req.flash('error_msg');
    res.locals.error = req.flash('error');//error passport messages (already named as 'error')*/
    res.locals.user = req.user || null;
    next();
});


//Routes
app.use(require('./routes/index.route.js'));
app.use(require('./routes/objects.route.js'));
app.use(require('./routes/roles.route.js'));
app.use(require('./routes/statistics.route.js'));

//Static files
app.use(express.static(path.join(__dirname,'public')));
app.use('/uploads',express.static(path.resolve('uploads')));


//CREATE FIRST ADMIN
const {createAdmin} = require('./controllers/role.controller');//Verifies if an administrator exists


//Server idle
app.listen(app.get('port'),process.env.SERVER_ADDRESSE ,()=>{
    console.log('Server connected on '+process.env.SERVER_ADDRESSE + ' :',app.get('port'));
    
},createAdmin);
