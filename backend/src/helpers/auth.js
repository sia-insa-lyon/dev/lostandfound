const jwt = require('jsonwebtoken')
const request = require("request");
const helpers = {};
const Role = require('../models/Role');

//Authentication verificaction (logged in)
helpers.isAuthenticated = (req,res,next) =>{
    if(req.isAuthenticated())
    next();
    else return res.status(401).json({success:false,message:'Unauthorized Request'});
  }


//Token verification
helpers.verifyToken = async(req, res, next) =>{
    try {
      if (!req.headers.authorization) {
        return res.status(401).send('Unauhtorized Request');
      }
      let token = req.headers.authorization.split(' ')[1];//Separate de real token of the header
      if (token === 'null') {
        return res.status(401).send('Unauhtorized Request');
      }
  
      const payload = await jwt.verify(token, process.env.AUTHO_TOKEN_SECRET_KEY);
      if (!payload) {
        return res.status(401).send('Unauhtorized Request');
      }
      req.userId = payload._id;//Getting the id and the role in the payload
      req.role = payload.role;
      next();
    } catch(e) {
      //console.log(e)
      return res.status(401).send('Unauhtorized Request');
    }
  }


//Moderator permission
helpers.isModerator =async (req,res,next) =>{ 
  const moderator = await Role.findById(req.user._id,{'role':1});
  if(moderator && moderator.role=='MODERATOR'){
    next();
  }else return res.status(403).json({success: false, message:'You don\'t have permission to do this action'});
}

//Admin permission
helpers.isAdmin = async(req,res,next) =>{  
  const admin = await Role.findById(req.user._id,{'role':1});
  if(admin && admin.role=='ADMIN'){
    next();
  }else return res.status(403).json({success: false, message:'You don\'t have permission to do this action'});
}

//Admin or moderator permission
helpers.isManager = async(req,res,next) =>{ 
  const manager = await Role.findById(req.user._id,{'role':1});
  if(manager && (manager.role=='ADMIN' ||manager.role=='MODERATOR')){
    next();
  }else return res.status(403).json({success: false, message:'You don\'t have permission to do this action'});
}

helpers.verifyCaptcha = async(req,res,next)=>{
  let token = req.body.recaptcha;
  const secretkey = process.env.CAPTCHA_SECRET_KEY; //the secret key from your google admin console;
  if(req.params.recaptcha){//identifies for by passing the recaptcha
    token = req.params.recaptcha
  }

  //token validation url is URL: https://www.google.com/recaptcha/api/siteverify 
  // METHOD used is: POST
  const url =  `https://www.google.com/recaptcha/api/siteverify?secret=${secretkey}&response=${token}&remoteip=${req.connection.remoteAddress}`
   
  //note that remoteip is the users ip address and it is optional
  // in node req.connection.remoteAddress gives the users ip address
  
  if(token === null || token === undefined){
    req.invalidCaptcha = "Token is empty or invalid";
    //res.status(201).send({success: false, message: "Token is empty or invalid"})
    //return console.log("token empty");
  }
  
  request(url, function(err, response, body){
    //the body is the data that contains success message
    body = JSON.parse(body);
    //console.log(body)
    //check if the validation failed
    if(body.success !== undefined && !body.success){
        req.invalidCaptcha = "recaptcha failed";
         //res.send({success: false, 'message': "recaptcha failed"});
         //return console.log("failed")
     }
    
    //if passed response success message to client
     next();
    
  })

}



module.exports = helpers;