//Statistics model

const mongoose = require('mongoose');
const { Schema } = mongoose;
const ObjectSchema = new Schema({
    entity:{type:String},
    active: { type: Number}, //lost object shown on "J'ai cherche un objet"
    found: { type: Number}, //object status after a user has found the owner
    //deleted: { type: Number }, //object status when the user deletes manually an object
    reported: { type: Number} , //object status when a manager reports an object because of inappropriate content or prank content
    expired: { type: Number}, //objects status when the object has not been found by the owner after a month
});

module.exports = mongoose.model('Statistics', ObjectSchema);