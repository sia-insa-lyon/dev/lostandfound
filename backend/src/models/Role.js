//Role model

const mongoose = require('mongoose');
const {Schema} = mongoose;
const bcrypt = require('bcryptjs');
const RoleSchema = new Schema({
    name: {type: String, required: true},
    lastname:{type:String,required:true},
    username: {type: String, required: true},
    password: {type: String, required: true},
    role:{type:String,required: true},//Role type:   ADMIN    |    MODERATOR
    date:{type: Date, default: Date.now},
});

//Encrypt the password
RoleSchema.methods.encryptPassword = async (password)=>{
    const salt = await bcrypt.genSalt(10);
    const hash = bcrypt.hash(password,salt);
    return hash;
};

RoleSchema.methods.matchPassword = async function (password){
    return await bcrypt.compare(password,this.password);
};

module.exports = mongoose.model('Role', RoleSchema);