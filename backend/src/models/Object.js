//Object model
const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2'); //Paginates documents
const { Schema } = mongoose;
const ObjectSchema = new Schema({
    type: { type: String, required: true }, //From 1 - 5:    "Carte" 1, "Téléphone" 2 , "Clefs" 2 , "Sac à dos" 3 , "Portefeuille" 4, "Autres"  5
    latitude: { type: Number, required: true },
    longitude: { type: Number, required: true } ,
    description: { type: String, required: true },
    imagePath:{type:String,required:true},
    name: {type: String, required: true},
    email:{type:String,required:true},
    foundcode:{type:String,required:true},
    date: { type: Date, default: Date.now }}
);

ObjectSchema.plugin(mongoosePaginate);
module.exports = mongoose.model('Objet', ObjectSchema);