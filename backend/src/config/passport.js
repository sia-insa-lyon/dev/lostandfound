const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const Role = require('../models/Role');

//User validation
passport.use(new LocalStrategy({
    usernameField: 'email'
}, async (email, password, done) => {
    const errors = [];
    const role = await Role.findOne({ username: email });//Role verification
    if (role) {
        const matchRole = await role.matchPassword(password);
        if (matchRole) {
            //console.log("Buena aconttraseña de admin de tipo: " + rol.type)
            return done(null, role);

        } else {
            errors.push({text: 'Mauvais mot de passe pour l\'administrateur/modérateur'});
            return done(null, false, { message: errors });
        }
    } else {
        errors.push({text: 'Nom d\'utilisateur inexistant'});
        return done(null, false, { message: errors });
    }
}));

//Keep a logged account
passport.serializeUser((user, done) => {//If the user is logged we save his id 
    //console.log(user); //User/Adminitrator/Modarator data
    done(null, user.id);
});

passport.deserializeUser(async (id, done) => {//
    await Role.findById(id, async (err, user) => {
        if (user) {
            //console.log("Administrator/Moderator logged in");
            done(err, user);
        } 
    });
});
