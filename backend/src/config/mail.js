//MAILING TEST (for now it is not implemented)

const nodemailer = require("nodemailer");
const { google } = require("googleapis");
var format = require('date-fns/format')//Date formatting
const OAuth2 = google.auth.OAuth2;

const emailCtrl = {};

const oauth2Client = new OAuth2(
    process.env.CLIENT_ID,
    process.env.CLIENT_SECRET_KEY, // Client Secret
    "https://developers.google.com/oauthplayground" // Redirect URL
);

oauth2Client.setCredentials({
    refresh_token: process.env.REFRESH_TOKEN
});
const accessToken = oauth2Client.getAccessToken()

const smtpTransport = nodemailer.createTransport({
    service: "gmail",
    auth: {
        type: "OAuth2",
        user: process.env.EMAIL,
        clientId: process.env.CLIENT_ID,
        clientSecret: process.env.CLIENT_SECRET_KEY,
        refreshToken: process.env.REFRESH_TOKEN,
        accessToken: accessToken
    }
});

emailCtrl.MailToPossibleOwner = async (data) => {
    const { owner, finder } = data;
    let htmlContent = `Hello! , <p>Voici le moyen de contact de la personne qui a probablement trouvé votre objet.</p>
    <p><strong>Mail: </strong> <a href="mailto:${finder}?Subject=C'est mon objet!" target="_blank">${finder}</a></p>
    <p>Cordialement.</p>
    <p>Lost and Found</p>`;
    const mailOptions = {
        from: `"Lost n\' Found" <${process.env.EMAIL}>`,
        to: owner,
        subject: "Contact",
        generateTextFromHTML: true,
        html: htmlContent
    };

    smtpTransport.sendMail(mailOptions, (error, response) => {
        error ? console.log(error) : console.log(response);
        smtpTransport.close();
    });

}

emailCtrl.MailToFinder = async (data) => {
    const { owner, name, finder, number, dial } = data;
    let htmlContent = `<b>Hey,  ${name} !</b> <p>Apparement quelqu'un a identifié l'objet que vous avez publié.</p>
    <p>Voici les moyens de contact de la personne:</p>
    <p><strong>Mail:</strong> <a href="mailto:${owner}?Subject=J'ai trouvé votre objet!" target="_blank">${owner}</a></p>`
    if (number) {
        htmlContent += `<p><strong>Numéro de téléphone:</strong> ${dial} ${number}</p>`
    }
    htmlContent += `<p>Demandez plus de details de l'objet afin de bien identifier le propiétaire.</p>
    <p>N'oubliez pas déclarer l'objet comme «Retrouvé par le propiétaire» avec le code recu lors de publication de l'objet. </p>
    <p>Cordialement.</p>
    <p>Lost and Found</p>`
    const mailOptions = {
        from: `"Lost n\' Found" <${process.env.EMAIL}>`,
        to: finder,
        subject: "Quelqu'un a identifié l'objet!",
        generateTextFromHTML: true,
        html: htmlContent
    };

    smtpTransport.sendMail(mailOptions, (error, response) => {
        error ? console.log(error) : console.log(response);
        smtpTransport.close();
    });

}

emailCtrl.objectPosted = async (data) => {
    const { receiver, type, date, name, foundcode } = data;
    var objectArray = process.env.OBJECT_ARRAY.split(',');
    const mailOptions = {
        from: `"Lost n\' Found" <${process.env.EMAIL}>`,
        to: receiver,
        subject: "Objet publié",
        generateTextFromHTML: true,
        html: `Hey,  <b>${name} !</b> <p>Vous venez de publier un objet de type <strong>${objectArray[type - 1]}</strong>.</p>
        <p>L'objet restera visible sur le site les ${process.env.TIME_BEFORE_EXPIRATION} prochains jours à partir d'aujourd'hui (${format(date, 'dd/MM/yyyy')}).</p>
        <p>Une fois que l'objet sera avec son propiétaire, veuillez déclarer l'objet comme «Retrouvé par le propiétaire» (en haut à droite en vert). Pour y faire, insérez ce code: <b>${foundcode}</b></p><p>Cela nous nous aidera à avoir les informations à jour et à évaluer l'utilité du site.</p>
        <p>Cordialement.</p>
        <p>Lost and Found</p>`
    };

    smtpTransport.sendMail(mailOptions, (error, response) => {
        error ? console.log(error) : console.log(response);
        smtpTransport.close();
    });
}

module.exports = emailCtrl;