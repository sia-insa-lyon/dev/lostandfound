const multer = require('multer');
const { v4: uuidv4 } = require('uuid');//random name
const path = require('path')


const storage = multer.diskStorage({
    destination: 'uploads',//Save images in uploads folder
    filename: (req, file, cb) => {
        cb(null, uuidv4() + path.extname(file.originalname)) //Save the image
    }
})


module.exports = multer({
    storage, limits: {
        fileSize: process.env.IMAGE_SIZE_MB_LIMIT*1000000 //Validate image size of 5 MB
    },
    fileFilter: (req, file, cb) => { //Filter only .jpg/.jpeg/.png files
        //console.log(file)
        if (path.extname(file.originalname).toUpperCase() !== '.JPG' && path.extname(file.originalname).toUpperCase() !== '.JPEG' && path.extname(file.originalname).toUpperCase() !== '.PNG') {
            req.fileValidationError = 'Photo pas valide';
            return cb(null, false, new Error('goes wrong on the mimetype'));
           
        }
        //console.log(getFilesizeInBytes(file.originalname))
        return cb(null, true);
    }
});